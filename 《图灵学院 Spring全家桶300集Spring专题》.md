# 几种定义Bean的方式

## P1 JavaBean、SpringBean、对象之间的区别

之前写的文章：[反向控制(Ioc)以及装配JavaBean方法的变革](https://www.yaodao666.xyz/archives/%E6%8E%A7%E5%88%B6%E5%8F%8D%E8%BD%AC)

Bean肯定是一个对象。

## P2 bean标签、@Bean、@Component定义Bean

- bean在Spring的xml中定义Bean   Spring读取类中的构造方法建造的对象
- @Bean在一个方法上   方法中new出来的一个对象
- @Component注解[还有其他注解]放在类上 注入一个Bean

## P3 BeanDefinition定义一个Bean

P2中都是反应式的方式来注册bean的，而它们的基础都是基于BeanDefinition的方法（编程式）来实现注册Bean的。

```java
/**
 * 通过BeanDefinition 的编程式方式来定义一个Bean
 */
AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
beanDefinition.setBeanClass(BeanDefinitionTest.class);
annotationConfigApplicationContext.registerBeanDefinition("beanDefinitionTest",beanDefinition);
annotationConfigApplicationContext.refresh();
BeanDefinitionTest beanDefinitionTest = annotationConfigApplicationContext.getBean("beanDefinitionTest",BeanDefinitionTest.class);
System.out.println("通过BeanDefinition注册的Bean"+beanDefinitionTest);
```

## P4 FactoryBean的使用和理解

通过实现FactoryBean接口，并重写其中的方法。这与之前学到的通过工厂方法来实现Bean的定义的方法不同。之前的是：

```java
public class ShopFactory {
    public static Shop getShop(){
    	Shop shop = new Shop();
    	return shop;
    }
}
```

```java
<bean id="shop" class="xxx.ShopFactory" factory-method="getShop" />
```

（或者不用xml方式而是基于Java的配置。）

而视频中讲的是：

```java
package com.yaodao.decorationdesign.object.dto;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

@Component
public class YaoDaoFactoryBean implements FactoryBean {

    //重写该方法，会返回一个名字为factoryBean的bean
    @Override
    public Object getObject() throws Exception {
        BeanDefinitionTest factoryBean = new BeanDefinitionTest();
        return factoryBean;
    }

    @Override
    public Class<?> getObjectType() {
        return BeanDefinitionTest.class;
    }
}
```

```java
/**
 * 通过FactoryBean获得Bean
 */
ApplicationContext ctx = new AnnotationConfigApplicationContext(YaoDaoFactoryBean.class);
BeanDefinitionTest beanDefinitionTest = ctx.getBean(BeanDefinitionTest.class);
System.out.println(beanDefinitionTest);
```

两种方法都差不多，甚至和之前讲的@Bean的方法都很像，可见Spring装配Bean的方法真是多种多样，不过就算是多种多样，也应该是封装成多种多样的，其核心应该是一样的，希望继续往下看能看到相关内容。

PS："先说下FactoryBean和其作用再开始分析：首先它是一个Bean，但又不仅仅是一个Bean。它是一个能生产或修饰对象生成的工厂Bean，类似于设计模式中的工厂模式和装饰器模式。它能在需要的时候生产一个对象，且不仅仅限于它自身，它能返回任何Bean的实例。"

## P5 Supplier的使用和定义Bean的方式

Spring提供registerBean方法，将P4中的方式进行封装，以一种简单的方式注册Bean。

```java
/**
 * 封装BeanDefinition的方法，直接注册
 */
AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
annotationConfigApplicationContext.registerBean(BeanDefinitionTest.class);
annotationConfigApplicationContext.refresh();
BeanDefinitionTest beanDefinitionTest = annotationConfigApplicationContext.getBean("beanDefinitionTest",BeanDefinitionTest.class);
System.out.println(beanDefinitionTest);
```

更高级一点：

```java
/**
 * 封装BeanDefinition的方法，直接注册
 */
AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
annotationConfigApplicationContext.registerBean(BeanDefinitionTest.class, new Supplier<BeanDefinitionTest>() {
    @Override
    public BeanDefinitionTest get() {
        BeanDefinitionTest supplierBeanDefinitionTest = new BeanDefinitionTest();
        supplierBeanDefinitionTest.setName("这是Supplier的Bean！");
        return supplierBeanDefinitionTest;
    }
});

annotationConfigApplicationContext.refresh();
BeanDefinitionTest beanDefinitionTest = annotationConfigApplicationContext.getBean("beanDefinitionTest",BeanDefinitionTest.class);
System.out.println(beanDefinitionTest);
```

获取Bean的时候用beanDefinitionTest是因为Spring默认名字是根据传入的类名(BeanDefinitionTest.class)决定的。

# Spring容器相关

## P6 单例池、单例Bean、单例模式的区别

单例Bean等同于Spring容器中只有一个某个类的对应的Bean么？ 并不。

```xml
<bean id = "user1" class = "User"/>
<bean id = "user2" class = "User"/>
```

比如上述注入User类对应的两个Bean，输出打印结果后发现user1与user2并不一样。

容器中的单例概念是作用在注册的Bean上，而不是类上。

## P7 单例池的结构和作用

与单例概念不同的是原型的概念。Spring容器默认是单例模式来实现Bean的，如上述标签对中的定义方式。

```xml
<bean id = "user1" class = "User" scope = "prototype"/>
<bean id = "user2" class = "User"/>
```

如果对bean注册时，scope(作用域)的值为prototype时，则是代表注册的是原型模式的Bean。

在Spring的官方文档中是这么描述以上两种模式的。

- 单例模式

  > Only one *shared* instance of a singleton bean is managed, and all requests for beans with an id or ids matching that bean definition result in that one specific bean instance being returned by the Spring container.

![singleton](https://gitee.com/yaodao666/blog-image/raw/master/singleton.png)

- 原型模式

  > The non-singleton, prototype scope of bean deployment results in the *creation of a new bean instance* every time a request for that specific bean is made.

![prototype](https://gitee.com/yaodao666/blog-image/raw/master/prototype.png)

单例模式很好理解，关于原型模式，文档中有一句话说的特别好

> In some respects, the Spring container’s role in regard to a prototype-scoped bean is a replacement for the Java `new` operator.

有些时候，可以把原型模式当作Java的new操作。即每次getBean的时候都相当于new一个Java对象——一个不同的Bean。

这就是为啥视频中当把user1换成原型模式后，两次getBean获得的Bean不一样的原因了。

单例池其实就是一个保存单份Bean的池子，在Spring程序运行时就加载了，并将非懒加载的单例Bean放到容器中。它的底层是ConcurrentHashMap。SingletonObject : 
	key : beanName
	value : Object
这种Map的保存方式就是保持单例的原因。对于一些懒加载的单例Bean则是啥时候用了就啥时候放到容器中。

## P8 初始BeanFactory

Bean工厂-->生产Bean的。

> BeanFacotry是spring中比较原始的Factory。如XMLBeanFactory就是一种典型的BeanFactory。原始的BeanFactory无法支持spring的许多插件，如AOP功能、Web应用等。 
> ApplicationContext接口(应用程序上下文-IOC容器),它由BeanFactory接口派生而来，ApplicationContext包含BeanFactory的所有功能，通常建议比BeanFactory优先

视频中一开始提到的DefaultListableBeanFactory也是BeanFactory的一个实现类。

前面的视频中还提到了一个FactoryBean，特意找了一篇不错的文章：[链接](https://blog.csdn.net/u010853261/article/details/72783361)

摘抄：

- BeanFactory

> BeanFactory是IOC最基本的容器，负责**生产和管理bean**，它为其他具体的IOC容器实现提供了最基本的规范，例如DefaultListableBeanFactory, XmlBeanFactory, ApplicationContext 等具体的容器都是实现了BeanFactory，再在其基础之上附加了其他的功能。

下面可以看看BeanFactory提供的基本功能：

```java
public interface BeanFactory {    
    String FACTORY_BEAN_PREFIX = "&";    
    Object getBean(String name) throws BeansException;    
    <T> T getBean(String name, Class<T> requiredType) throws BeansException;    
    <T> T getBean(Class<T> requiredType) throws BeansException;    
    Object getBean(String name, Object... args) throws BeansException;    
    boolean containsBean(String name);    
    boolean isSingleton(String name) throws NoSuchBeanDefinitionException;    
    boolean isPrototype(String name) throws NoSuchBeanDefinitionException;    
    boolean isTypeMatch(String name, Class<?> targetType) throws NoSuchBeanDefinitionException;    
    Class<?> getType(String name) throws NoSuchBeanDefinitionException;    
    String[] getAliases(String name);    
} 
```

- FactoryBean

> FactoryBean是一个接口，当在IOC容器中的Bean实现了FactoryBean接口后，通过getBean(String BeanName)获取到的Bean对象并不是FactoryBean的实现类对象，而是这个实现类中的getObject()方法返回的对象。要想获取FactoryBean的实现类，就要getBean(&BeanName)，在BeanName之前加上&。

FactoryBean源码:

```java
public interface FactoryBean<T> {    
    T getObject() throws Exception;    
    Class<?> getObjectType();    
    boolean isSingleton();    
}  
```

同时这里还有一篇不错的关于IOC容器 BeanFactory源码解析的博文，分享：[链接](https://blog.csdn.net/bawcwchen/article/details/79771412)

视频中有一点讲的很好：BeanFactory就是一个容器，它可以通过注册BeanDefinition的方式注册Bean，或者直接注册Bean对象，而后者只是前者的封装。

![image-20210215225208343](https://gitee.com/yaodao666/blog-image/raw/master/image-20210215225208343.png)

## P9 ApplicationContext与BeanFactory的联系和区别

```java
public interface ApplicationContext extends EnvironmentCapable, ListableBeanFactory, HierarchicalBeanFactory, MessageSource, ApplicationEventPublisher, ResourcePatternResolver
```

ApplicationContext继承了ListableBeanFactory，而后者又继承了BeanFactory。

Application 继承了很多接口，集成了很多功能，例如：

- MessageSource 国际化
- ApplicationEventPublisher 事件发布

## P10 ApplicationContext应用(获取资源、发布事件、国际化)

主要讲的是，Application与BeanFactory非常的像，比如可以注册Bean，获得Bean等操作（包括BeanDefinition）。

其他功能：

- 获取系统的环境变量

```java
/**
 * ApplicationContext的一些功能
 */
//使用 AnnotationConfigApplicationContext实现ApplicationContext接口 用作测试实验
AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

// - 获取环境变量（System）
System.out.println(applicationContext.getEnvironment().getSystemEnvironment());
```

![image-20210216100048805](https://gitee.com/yaodao666/blog-image/raw/master/image-20210216100048805.png)

成功打印出系统变量信息。

- JVM变量信息

  ```java
  System.out.println(applicationContext.getEnvironment().getSystemProperties());
  ```

- 发布事件

![image-20210216100505937](https://gitee.com/yaodao666/blog-image/raw/master/image-20210216100505937.png)

- 获取资源

![image-20210216100551512](https://gitee.com/yaodao666/blog-image/raw/master/image-20210216100551512.png)

- 国际化  MessageSource

第一次参数是，第二个参数是一个Object[]，第三个参数是国家。

相关链接：https://blog.csdn.net/qq_45057072/article/details/106739437

## P11 Application按不同的角度进行分类

1.如何对Bean进行配置上的几个分类

- AnnotationConfigApplicationContext
- ClassPathXmlApplicationContext
- FileSystemXmlApplicationContext

2.按照是否可刷新区分

## P12 ClassPathXmlApplicationContext和FileSystemXmlApplicationContext

可以看自己之前的博客：[链接](https://www.yaodao666.xyz/archives/%E6%8E%A7%E5%88%B6%E5%8F%8D%E8%BD%AC)

![image-20210216220915179](https://gitee.com/yaodao666/blog-image/raw/master/image-20210216220915179.png)

视频中主要讲了二者的不同。

第二种方式使用的是文件的路径，支持绝对路径和相对路径。而前者则是类路径，是类的相对路径。例如上图中，直接写了SpringBean.xml就是因为xml文件和Test2类在同一个路径下。

![image-20210216221257828](https://gitee.com/yaodao666/blog-image/raw/master/image-20210216221257828.png)

## P13 AnnotationConfigApplicationContext

![image-20210216221001303](https://gitee.com/yaodao666/blog-image/raw/master/image-20210216221001303.png)

使用注解（即Java的方式来装配Bean）时，就可以用AnnotationConfigApplicationContext来获得容器。

## P14 可刷新的和不可刷新的ApplicationContext

可刷新和不可刷新的区别的直观体现就是 applicationContext是否能调用refresh方法。

AbstractApplicationContext接口下实现了AbstractRefreshableApplicationContext和GenericApplicationContext，分别是两个可刷新的容器和不可刷新的容器。

于是继承了前者的实现类就具有刷新功能，继承了后者的反之。

- 继承AbstractRefreshableApplicationContext的：

  ![image-20210216111413751](https://gitee.com/yaodao666/blog-image/raw/9d7c76a6e94ab17568631d8182e218442da79c8f/image-20210216111413751.png)

- 继承GenericApplicationContext的：

![image-20210216111443038](https://gitee.com/yaodao666/blog-image/raw/489a3b2b812d94c94683b26edae3ef8b1ab44260/image-20210216111443038.png)

refresh的作用？

相当于重启了容器，先将容器中的内容（BeanDefinition和Bean）清空掉，然后根据target下的已经的xml文件或者编译好的class文件来进行Bean的注入等操作，就相当于电脑系统重启是一样的，把内存清空，根据磁盘内容重新载入。所以视频中的两次getBean获得的bean是不一样的。

# Bean的生命周期

## P15 Bean的生命周期概述

生成Bean实际上就是new一个对象、填充属性。

## P16 Bean的后置处理器介绍

对象**实例化之后**与该对象变成Bean之前会经过Bean的后置处理器并执行其中的两个方法：

- postProcessBeforeInitialization
- postProcessAfterInitialization

```java
/**
 * @Author: YaoDao
 * @Package: Bean的后置处理器
 * @Description:
 * @Date: 2021/1/28 8:17
 */
@Slf4j
@Configuration
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        log.info("我是：postProcessBeforeInitialization");
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        log.info("我是：postProcessAfterInitialization");
        return null;
    }
}
```

并且是先执行Before再执行After。当重新这俩个方法使得return的对象不为空（默认返回空）时，则Bean就会return的对象，反之根据Bean注册时的配置该怎样就怎样。

当我去执行SpringBoot的启动方法时（就会将项目中所有的Bean加载），打印结果如下：

每一个Bean的加载都会调用上述的两个方法。

![image-20210217213246029](https://gitee.com/yaodao666/blog-image/raw/master/image-20210217213246029.png)

看代码中两个方法的参数，一个是对象，一个是Bean的名字。可以对其做一些操作。

注意：后置处理器是针对所有Bean的，所以如果是想对某一个Bean创建前通过处理器做一些特殊操作，一定记得加一些判断条件，例如通过参数beanName做判断。

## P17 Bean的后置处理器有趣的实战

利用后置处理器来进行对象的属性赋值，以实现一个与Spring提供的@Value注解功能一样的注解。

- 利用注解标注一个Bean组件

```java
/**
 * @Author: YaoDao
 * @Package:
 * @Description: 只是用来学习用的一个类  用于后置处理器来实现注解来为该类中的属性赋值
 * @Date: 2021/2/19 8:38
 */
@Component
@Data
public class AnAnnotationBean {
    @YaoDaoValue("default")
    private String name;
}
```

- 实现YaoDaoValue注解

```java
/**
 * @Author: YaoDao
 * @Package:
 * @Description: 一个跟@Value功能相似的注解  用于学习后置处理器用的
 * @Date: 2021/2/19 8:47
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface YaoDaoValue {
    String value();
}
```

- 写后置处理器中对属性赋值的逻辑

```java
/**
 * @Author: YaoDao
 * @Package: bean的后置处理器
 * @Description:
 * @Date: 2021/1/28 8:17
 */
@Slf4j
@Configuration
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        //log.info("我是：postProcessBeforeInitialization");
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        //log.info("我是：postProcessAfterInitialization");
        if("anAnnotationBean".equals(beanName)){
            //如果是anAnnotationBean，那么就为其属性赋值
            Class<?> aClass = bean.getClass();
            for (Field declaredField : aClass.getDeclaredFields()) {
                if (declaredField.isAnnotationPresent(YaoDaoValue.class)) {
                    //如果这个Bean中的某个属性上有这个注解，那就为该属性赋值
                    YaoDaoValue annotation = declaredField.getAnnotation(YaoDaoValue.class);
                    //拿到这个注解的值。
                    String value = annotation.value();
                    declaredField.setAccessible(true);
                    //field.setAccessible(true);的作用就是允许我们用反射访问该私有变量
                    //通过反射去赋值
                    try {
                        //向该对象中的当前字段赋值为value
                        declaredField.set(bean,value);
                        log.info("成功为anAnnotationBean中的name字段赋值为{}",value);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            return bean;
        }
        return null;
    }
}
```

- 容器中获得该Bean并打印

```java
public class DecorationDesignApplication {

    public static void main(String[] args) {
		//SpringApplication.run(DecorationDesignApplication.class, args);
        //获取容器
        ConfigurableApplicationContext run = SpringApplication.run(DecorationDesignApplication.class, args);
        System.out.println(run.getBean("anAnnotationBean"));
 }
```

结果：

![image-20210219105036831](https://gitee.com/yaodao666/blog-image/raw/master/image-20210219105036831.png)

关于反射的一些知识：[反射机制详解](https://mp.weixin.qq.com/s/YFmaCZG_y21RX7lgjipeaA)

## P18 生命周期之实例化前

事实上Bean的创建分为以下几步：

![image-20210219125816388](https://gitee.com/yaodao666/blog-image/raw/master/image-20210219125816388.png)

实例化前就是指已经获取到了BeanDefinition信息，还没有创建出对象的时候。

通过实现BeanPostProcessor的子接口InstantiationAwareBeanPostProcessor，来实现实例化前的一些操作，视频中没做一些演示，这里也就只把代码贴出来：

```java
package com.yaodao.decorationdesign.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: YaoDao
 * @Package: 实现BeanPostProcessor的一个子接口，实现Bean的生命周期之实例化前的相关操作
 * @Description:
 * @Date: 2021/2/19 13:03
 */
@Configuration
public class MyBeanPostProcessorBeforeInit implements InstantiationAwareBeanPostProcessor {

    /**
     * create by: YaoDao
     * description: 这是实例化前的方法，不同于postProcessBeforeInitialization 根本不会有 Object bean参数
     * 只有类信息以及BeanDefinition中的beanName
     * create time: 2021/2/19 13:08
     * @return
     */
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        return null;
    }
}
```



## P19 生命周期之实例化和推断构造方法

当一个类通过视频中的方法（添加@Component）作为Bean时，就会采用基于构造器的方法来进行实例化。如果该类没有显示的构造方法，实例化时会调用无参的构造方法，如果有一个显式的构造方法则会调用该构造方法来进行实例化。当有一个以上的构造方法且均为有参的构造方法时就会报错啦。只要有显式无参的构造方法就一定会调用无参构造方法来实例化。

视频中的UserService使用了User类，在Spring中通过Bean注入的形式来实现，这就是所谓的依赖注入。视频中演示的只是通过基于构造器的方法实现的注入，实际上还有通过基于setter方法和工厂方法的方式来完成实例化，在此就不详细说明了。可以看链接：[Dependency Injection](https://docs.spring.io/spring-framework/docs/4.3.30.RELEASE/spring-framework-reference/htmlsingle/#beans-constructor-injection).

我在工作中（我的小伙伴也是）常常用一个标记在私有属性上的@Autowired注解就能完成属性的注入，根本不需要写有参构造方法——如下：

```java
@Autowired
private User user;
```

在Spring 4.3.30的官方文档中并没有提及这一点，好像也并不推荐这种方法，但确实是很方便的。

视频中还谈到了，将@Autowired注解加到构造方法上，此时，Spring实现bean的实例化的时候调用的构造器就是Autowired标注的构造方法了——同理不可以给两个构造方法上都加一个@Autowired。该注解通过参数required的值来控制@Autowired是否必须生效。默认required为true，当为false时表示为可选的，当没有为true的@Autowired时，选择权可以交给Spring来选择——视频讲的Spring是如何选择的，这个作为了解就好。

## 小插曲-Spring官网基于XML进行依赖注入的三种方法

Spring官方提供了三种注册Bean的方法（其中包括构造器，与视频中不同的是，视频中使用的编程式的注入，官方是使用XML文件声明式的注入）。下面是几个官网提供的几个例子：

#### Examples of dependency injection

The following example uses XML-based configuration metadata for setter-based DI. A small part of a Spring XML configuration file specifies some bean definitions:

##### setter

```xml
<bean id="exampleBean" class="examples.ExampleBean">
    <!-- setter injection using the nested ref element -->
    <property name="beanOne">
        <ref bean="anotherExampleBean"/>
    </property>

    <!-- setter injection using the neater ref attribute -->
    <property name="beanTwo" ref="yetAnotherBean"/>
    <property name="integerProperty" value="1"/>
</bean>

<bean id="anotherExampleBean" class="examples.AnotherBean"/>
<bean id="yetAnotherBean" class="examples.YetAnotherBean"/>
```

```java
public class ExampleBean {

    private AnotherBean beanOne;

    private YetAnotherBean beanTwo;

    private int i;

    public void setBeanOne(AnotherBean beanOne) {
        this.beanOne = beanOne;
    }

    public void setBeanTwo(YetAnotherBean beanTwo) {
        this.beanTwo = beanTwo;
    }

    public void setIntegerProperty(int i) {
        this.i = i;
    }
}
```

##### constructor

```xml
<bean id="exampleBean" class="examples.ExampleBean">
    <!-- constructor injection using the nested ref element -->
    <constructor-arg>
        <ref bean="anotherExampleBean"/>
    </constructor-arg>

    <!-- constructor injection using the neater ref attribute -->
    <constructor-arg ref="yetAnotherBean"/>

    <constructor-arg type="int" value="1"/>
</bean>

<bean id="anotherExampleBean" class="examples.AnotherBean"/>
<bean id="yetAnotherBean" class="examples.YetAnotherBean"/>
```

```java
public class ExampleBean {

    private AnotherBean beanOne;

    private YetAnotherBean beanTwo;

    private int i;

    public ExampleBean(
        AnotherBean anotherBean, YetAnotherBean yetAnotherBean, int i) {
        this.beanOne = anotherBean;
        this.beanTwo = yetAnotherBean;
        this.i = i;
    }
}
```

##### static factory

```xml
<bean id="exampleBean" class="examples.ExampleBean" factory-method="createInstance">
    <constructor-arg ref="anotherExampleBean"/>
    <constructor-arg ref="yetAnotherBean"/>
    <constructor-arg value="1"/>
</bean>

<bean id="anotherExampleBean" class="examples.AnotherBean"/>
<bean id="yetAnotherBean" class="examples.YetAnotherBean"/>
```

```java
public class ExampleBean {

    // a private constructor
    private ExampleBean(...) {
        ...
    }

    // a static factory method; the arguments to this method can be
    // considered the dependencies of the bean that is returned,
    // regardless of how those arguments are actually used.
    public static ExampleBean createInstance (
        AnotherBean anotherBean, YetAnotherBean yetAnotherBean, int i) {

        ExampleBean eb = new ExampleBean (...);
        // some other operations...
        return eb;
    }
}
```



## P20 生命周期之实例化后

![image-20210221200539476](https://gitee.com/yaodao666/blog-image/raw/master/image-20210221200539476.png)

```java
@Configuration
public class MyBeanPostProcessorAfterInit implements InstantiationAwareBeanPostProcessor {
    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        return false;
    }
}

```

这个方法的作用和P16讲的很像，只不过是返回内容不同，对实例化对象的影响范围不同。在P16中讲的是已经实例化出一个对象了，可以为该对象进行属性填充甚至重新实例化一个别的对象来替换掉该对象，而本节讲的postProcessAfterInstantiation方法却只能对该对象的属性值进行操作，权限范围只局限于传来的对象。通过返回一个布尔型的值，来作为一个开关，返回false时是告诉Spring不需要它再为该属性注入值了，反之Spring可以调用一些方法比如Set方法继续进行属性赋值。

## P21 生命周期之初始化-属性注入

> InitializinBean - 该接口中有一个afterPropertiesSet方法。会在当前继承了该接口的**Bean对象**实例化后并且属性注入完之后执行。

```java
@Component
@Data
public class AnAnnotationBean implements InitializingBean {
    @YaoDaoValue("default")
    private String name;

    @Override
    public void afterPropertiesSet() throws Exception {
        
    }
}
```

Javax包中有一个注解可以替代上面的afterPropertiesSet方法，就不用实现接口中的方法了。

```java
@PostConstruct
public void afterPropertiesSet() {

}
```

视频中还讲了这两种方法在原理上的稍微的不同点，即前者是在初始化时执行，后者是在初始化前执行，但都是在属性注入之后执行的。

## P22 生命周期之初始化前

> InstantiationAwareBeanPostProcessor这个接口是老熟人了，不光有关于实例化前后的方法，还有初始化前后的方法。

![image-20210221204025166](https://gitee.com/yaodao666/blog-image/raw/master/image-20210221204025166.png)

```java
@Configuration
public class MyBeanPostProcessorBeforeChuShi implements InstantiationAwareBeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        //xxx操作
        return null;
    }
}
```

如果这里返回的是null，那么原先在Bean的类中通过@PostConstruct注解标注的方法就不会再执行了，反之如果返回的还是参数中传来的bean，那么该bean中的通过@PostConstruct注解标注的方法就会继续执行。视频中老师还用了反射来在postProcessBeforeInitialization方法中特意去执行类中的@PostConstruct注解标注的方法，比较有趣的还是，毕竟反射用的真的不多。

```java
//初始化前
@Override
public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
    if (beanName.equals("xxx")){
        for (Method method : bean.getClass().getMethods()) {
            if (method.isAnnotationPresent(PostConstruct.class)){
                try {
                    method.invoke(bean);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    return null;
}
```

## P23 生命周期之初始化后以及模拟AOP

![image-20210222111529719](https://gitee.com/yaodao666/blog-image/raw/master/image-20210222111529719.png)

```java
//初始化后
@Override
public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    return null;
}
```

视频中讲的跟前几节内容相近。

关于Proxy.newProxyInstance：

```
java在JDK1.5之后提供了一个"java.lang.reflect.Proxy"类，通过"Proxy"类提供的一个newProxyInstance方法用来创建一个对象的代理对象
```

链接：https://www.cnblogs.com/wobuchifanqie/p/9991342.html

## Spring与Mybatis

## P24 标题与内容不符 Spring-Mybatis

讲的是SpringBoot整合Mybatis的东西。

## P25 标题与内容不符  映射器注入 

讲的是如何向其他Bean中注入映射器Mapper。[官方教程](http://mybatis.org/spring/zh/mappers.html)

这跟前面讲的如何依赖注入Bean差不多，毕竟映射器也是一个Bean。

## P26 标题与内容不符  映射器注入 

还是在讲如何向其他Bean中注入映射器Mapper。无法正常将Mapper注入到所需要该Mapper的bean中，就是视频中演示的错误。这一节就是为了排错的。

```java
@Autowired
private UserMapper userMapper;
```

如何将UserMapper注入？Spring中首先要有这个UserMapper类的Bean对象（这部分原理请看第P19的笔记）。

报错的原因就是因为没有把UserLoginMapper类注册为Bean。而UserMapper实际上是一个接口，它的bean对象怎么整呢？——接口的代理对象（P23）

这个代理的对象是谁来生成的呢？Spring or Mybatis？

```
mybatis来生成Mapper的代理对象。
```

mybatis也是一个框架，是可以单独使用的，不是非要和Spring组合使用的。是可以通过getMapper方法来获得这个对象的。

## P27 Spring整合Mybatis的核心底层原理 +P28+P29

接上一节，既然Mabatis能够自己产生Mapper接口的代理对象，那么只要把这个对象放到Spring容器中就好了。这其实就是Spring整合Mybatis的关键所在。

在前面的章节中讲了一个bean是由类通过构造器来进行实例化的，而Mapper特殊就在于他不是一个类而是一个接口，不能直接实例化。按照Spring正常注入bean的方式来注入是会报实例化错误的错误信息的。

视频中为了解决这个问题，建了一个类 实现Spring中的FactoryBean接口(P4中有讲这个办法来注册Bean)：

```java
public class LubanFactoryBean() implements FactoryBean{
    @Override
    public Object getObject() throws Exception {
        Object o = Proxy.newProxyInstance(LubanFactoryBean.class.getClassLoader(),new Class[] {UserMapper.class}, 
                   	new InvocationHandler(){
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        return null;
                    }
        });
        return o;
    }

    @Override
    public Class<?> getObjectType() {
        return UserMapper.class;
    }
}
```

FactoryBean是一个接口，当在IOC容器中的Bean实现了FactoryBean接口后，通过getBean(String BeanName)获取到的Bean对象并不是FactoryBean的实现类对象，而是这个实现类中的getObject()方法返回的对象。要想获取FactoryBean的实现类，就要getBean(&BeanName)，在BeanName之前加上&。

这个时候，这跟UserMapper的代理对象就以Bean的形式注册在Spring中了。

而实际上在开发时把这个对象加到容器中根本不需要这么麻烦的，直接在接口上加一个@Component的注解即可【Spring整合MyBatis会自动对此注解进行特殊操作】，当然老师这么写是为了告诉我们加入容器的底层代码。

## P30 ImportBeanDefinitionRegistrar和FactoryBean的经典实战

![image-20210303152437704](https://gitee.com/yaodao666/blog-image/raw/master/image-20210303152437704.png)

写两个类生成两个代理对象。并注册这些bean。 同样的 这两步可以用@Component注解完成。

不能每定义这么一个Mapper接口就去实现一个FactoryBean类吧？ 优化！

将传入的Mapper抽象化

```java
public class LubanFactoryBean() implements FactoryBean{
    
    private Class mapperClass;
    
    //使用构造器初始化mapperClass
    public LubanFactoryBean(Class mapperClass){
        this.mapperClass = mapperClass;
    }
    
    @Override
    public Object getObject() throws Exception {
        Object o = Proxy.newProxyInstance(LubanFactoryBean.class.getClassLoader(),new Class[] {mapperClass.class}, 
                   	new InvocationHandler(){
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        return null;
                    }
        });
        return o;
    }

    @Override
    public Class<?> getObjectType() {
        return mapperClass.class;
    }
}
```

回顾P19讲的内容，此时Spring会去调用上面的这个有参构造方法来实例化LuBanfactoryBean类。但是！此时的传入参数是Class类型的且该参数根本不是一个Bean，所以Spring根本不知道如何去找这个参数完成注入啊。==> **手动实现参数的注入！**

```java
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition.setBeanClass(LuBanFactoryBean.class);
        beanDefinition.getConstructorArgumentValues().addGenericArgumentValue(UserMapper.class);
        applicationContext.registerBeanDefinition("user1",beanDefinition);
```

beanDefinition.getConstructorArgumentValues().addGenericArgumentValue(UserMapper.class);手动加参数，学到了，但是应该是没啥用的...

## P31 MapperFactoryBean和SqlSessionFactoryBean的作用和底层代码

把上面注册Bean的代码放到一个类（实现ImportBeanDefinitionRegistrar接口）中，代码略。

只需要在启动类上加上一个@Import(类名.class)注解即可。

先明白我们这是在做什么？我们是在写MyBatis与Spring的中间件，以达到学习Spring到底是如何整合MyBatis的，而不是在用Spring提供的Mybatis的中间件。

继续抽象化注册的代码，首先要解决几个问题：

- 扫描路径
- 扫描到了接口却不能实例化[扫描的作用是为了找到一个类，并某种方式实例化注入到容器中，但是接口是不能实例化的，这也是之前为啥要用代理对象实例化Mapper的原因]

路径好解决，重写scan方法。

扫描方法却必须要该点东西。Spring的优秀扩展性为我们提供 更改扫描方式的逻辑。

```
继承 ClassPathBeanDefinitionScanner 类，重写isCandidateComponent方法：return beanDefinition.getMetadata().isInterface();
```

这一小节比较困难，但挺有意思。

以上几个小节就是Spring整合MyBatis的原理了，实际开发中自然是不需要这么麻烦的，但多理解理解底层还是有好处的。



# 手写Spring框架

先创建一个工程，用来写这个章节的代码  gitee地址：https://gitee.com/yaodao666/turing-spring-learning-notes.git

![image-20210303170657544](https://gitee.com/yaodao666/blog-image/raw/master/image-20210303170657544.png)

## P32 手写Spring的扫描逻辑+P33+P34+P35

模仿：

```java
ApplicationContext ctx = new AnnotationConfigApplicationContext(BeanConfig.class);
```

自己实现一个AnnotationConfigApplicationContext，有点意思。

- Test.java 充当启动类
- LuBanApplicationContext.java 充当容器类AnnotationConfigApplicationContext
- AppConfig Bean配置类
- @ComponentScan注解 充当Spring中的包扫描注解
- @Component注解 充当Spring中的@Component、@Service、@Confirguration等作用为标注当前类为组件的注解
- @Autowired注解 充当Spring中的@Autowired
- @Lazy 懒加载
- @Scope 单例 原型 或者其他
- BeanDefinition类

扫描到要创建的类[scan]->创建实例->加入Map中->可以getBean了

代码都写了，放在码云仓库中了。P35 19分钟之后的视频没有看，大概看了看没啥用。

## P36 什么是循环依赖+P37+P38

关键点：依赖只在Spring中存在。原因是，谁先初始化的问题。

一级缓存：单例池(看上一节的内容)

二级缓存：一个Map

三级缓存：也是Map的数据结构

当A、B循环依赖时，在A初始化后，但还没属性填充前就把A放到二级缓存中，然后A进行属性填充时因B还没初始化，就去初始化B并进行B的属性填充（此时二级缓存中已经有A的初始对象了），从而A的属性填充也完成了，A,B也都可以同时加入到一级缓存中（单例池）了。

问题：