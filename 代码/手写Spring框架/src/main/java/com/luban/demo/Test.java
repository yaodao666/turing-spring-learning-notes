package com.luban.demo;

import com.luban.demo.service.UserService;
import com.luban.spring.LuBanApplicationContext;

/**
 * 充当SpringBoot启动类
 */
public class Test {
    public static void main(String[] args) throws Exception {
        //模拟启动SpringBoot
        LuBanApplicationContext applicationContext = new LuBanApplicationContext(AppConfig.class);
        UserService userService = (UserService) applicationContext.getBean("userService");
        Object userService1 = applicationContext.getBean("userService");
        Object userService2 = applicationContext.getBean("userService");
        System.out.println(userService);
        System.out.println(userService1);
        System.out.println(userService2);
        userService.test();
    }
}
