package com.luban.demo;

import com.luban.spring.ComponentScan;

/**
 * 充当Bean配置类
 */
@ComponentScan("com.luban.demo.service")
public class AppConfig {

}
