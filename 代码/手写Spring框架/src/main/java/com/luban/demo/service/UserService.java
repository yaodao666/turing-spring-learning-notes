package com.luban.demo.service;

import com.luban.spring.Autowired;
import com.luban.spring.Component;
import com.luban.spring.Lazy;
import com.luban.spring.Scope;

/**
 * 一个普通的Bean
 */
@Component("userService")
//@Lazy
//@Scope("prototype")
public class UserService {
    @Autowired
    private User user;

    public void test(){
        System.out.println(user);
    }
}
