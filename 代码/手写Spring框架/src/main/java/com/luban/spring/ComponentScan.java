package com.luban.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 扫描注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) //该注解只能定义在/** Class, interface (including annotation type), or enum declaration */上
public @interface ComponentScan {
    //这个value用于充当扫描路径
    String value() default "";
}
